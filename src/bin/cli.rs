#![warn(clippy::all)]

use async_std::prelude::*;
use async_std::sync::{channel, Receiver, Sender};
use async_std::{fs, io, stream};
use cryptobox::{store::file::FileStore, CBox, CBoxError, CBoxSession};
use http::StatusCode;
use proteus::keys::PreKeyId;
use structopt::StructOpt;
use tide::{error::ResultExt, App, EndpointResult};

enum ThreadMessage {
  NeedsPrekeys,
  HasPrekeys(Vec<u8>),
  HasClientInput(String),
  HasServerInput(Vec<u8>),
}

struct ChatChannel {
  sender: Sender<ThreadMessage>,
  receiver: Receiver<ThreadMessage>,
}

#[derive(StructOpt, Debug)]
#[structopt(name = "asylum")]
struct Opt {
  #[structopt(short, long)]
  name: String,

  #[structopt(short, long)]
  port: u64,

  #[structopt(short, long)]
  address: String,

  #[structopt(short, long)]
  initiate: bool,
}

trait CBoxExt<S: cryptobox::store::Store> {
  fn ensure_prekeys(&self) -> Result<(), CBoxError<S>>;
}

impl<S: cryptobox::store::Store> CBoxExt<S> for CBox<S> {
  fn ensure_prekeys(&self) -> Result<(), CBoxError<S>> {
    // self.
    for i in 0..100 {
      self.new_prekey(PreKeyId::new(i))?;
    }

    Ok(())
  }
}

async fn get_prekeys(cx: tide::Context<ChatChannel>) -> EndpointResult<Vec<u8>> {
  // Need both of these assignments to prevent a compiler bug
  let state = cx.state();

  state.sender.send(ThreadMessage::NeedsPrekeys).await;

  while let Some(message) = state.receiver.recv().await {
    if let ThreadMessage::HasPrekeys(pre_key_bundle) = message {
      return Ok(pre_key_bundle);
    }
  }

  Err(tide::Error::from(StatusCode::INTERNAL_SERVER_ERROR))
}

async fn receive_message(mut cx: tide::Context<ChatChannel>) -> EndpointResult<()> {
  let ciphertext = cx.body_bytes().await.client_err()?;

  // Need both of these assignments to prevent a compiler bug
  let state = cx.state();

  state
    .sender
    .send(ThreadMessage::HasServerInput(ciphertext))
    .await;

  Ok(())
}

async fn get_stdin() -> Option<ThreadMessage> {
  // TODO: don't reallocate this all the time
  let mut input = String::new();
  let len = io::stdin().read_line(&mut input).await;
  let len = Result::ok(len).unwrap_or(0);
  if len > 0 {
    Some(ThreadMessage::HasClientInput(input))
  } else {
    None
  }
}

async fn start_session(
  opt: &Opt,
  cbox: CBox<FileStore>,
  sender: Sender<ThreadMessage>,
  receiver: Receiver<ThreadMessage>,
) -> Result<
  (
    CBoxSession<FileStore>,
    CBox<FileStore>,
    Sender<ThreadMessage>,
    Receiver<ThreadMessage>,
  ),
  std::io::Error,
> {
  let endpoint = format!("http://{}/prekeys", opt.address);
  if opt.initiate {
    let pre_key_bundle = surf::get(endpoint)
      .recv_bytes()
      .await
      .map_err(|err| std::io::Error::new(std::io::ErrorKind::Other, err))?;

    let session = cbox
      .session_from_prekey("yeet".to_string(), &pre_key_bundle)
      .map_err(|err| std::io::Error::new(std::io::ErrorKind::Other, err))?;

    return Ok((session, cbox, sender, receiver));
  }

  while let Some(message) = receiver.recv().await {
    match message {
      ThreadMessage::NeedsPrekeys => {
        let pre_key_bundle = cbox
          .new_prekey(PreKeyId::new(0))
          .map_err(|err| std::io::Error::new(std::io::ErrorKind::Other, err))?
          .serialise()
          .map_err(|err| std::io::Error::new(std::io::ErrorKind::Other, err))?;
        sender.send(ThreadMessage::HasPrekeys(pre_key_bundle)).await;
      }
      ThreadMessage::HasServerInput(ciphertext) => {
        let (session, plaintext) = cbox
          .session_from_message("yeet".to_string(), &ciphertext)
          .map_err(|err| std::io::Error::new(std::io::ErrorKind::Other, err))?;
        let plaintext = String::from_utf8(plaintext)
          .map_err(|err| std::io::Error::new(std::io::ErrorKind::Other, err))?;
        println!("{}", plaintext);
        return Ok((session, cbox, sender, receiver));
      }
      _ => {}
    }
  }

  Err(std::io::Error::new(
    std::io::ErrorKind::TimedOut,
    "The main thread terminated early.",
  ))
}

#[runtime::main]
async fn main() -> Result<(), std::io::Error> {
  let opt = Opt::from_args();

  let identity_path = format!("./{}", opt.name);
  fs::create_dir_all(identity_path).await?;

  let (send_to_client, recieve_from_server) = channel::<ThreadMessage>(65565);
  let (send_to_server, recieve_from_client) = channel::<ThreadMessage>(65565);

  let channel = ChatChannel {
    sender: send_to_client,
    receiver: recieve_from_client,
  };

  let mut app = App::with_state(channel);

  // Start the server
  let addr = format!("0.0.0.0:{}", opt.port);
  std::thread::spawn(|| {
    app.at("/prekeys").get(get_prekeys);
    app.at("/inbox").post(receive_message);
    app.run(addr)
  });

  let inbox_endpoint = format!("http://{}/inbox", opt.address);

  let cbox = CBox::file_open(&opt.name)
    .map_err(|err| std::io::Error::new(std::io::ErrorKind::NotFound, err))?;

  let (mut session, cbox, send_to_server, recieve_from_server) =
    start_session(&opt, cbox, send_to_server, recieve_from_server).await?;

  let stdin_stream = stream::from_fn(get_stdin);
  let mut main_stream = Box::pin(stdin_stream.merge(recieve_from_server));

  while let Some(message) = main_stream.next().await {
    match message {
      ThreadMessage::NeedsPrekeys => {
        let pre_key_bundle = cbox
          .new_prekey(PreKeyId::new(0))
          .map_err(|err| std::io::Error::new(std::io::ErrorKind::Other, err))?
          .serialise()
          .map_err(|err| std::io::Error::new(std::io::ErrorKind::Other, err))?;
        send_to_server
          .send(ThreadMessage::HasPrekeys(pre_key_bundle))
          .await;
      }
      ThreadMessage::HasClientInput(text) => {
        let ciphertext = session
          .encrypt(&text.as_bytes())
          .map_err(|err| std::io::Error::new(std::io::ErrorKind::Other, err))?;
        surf::post(&inbox_endpoint)
          .body_bytes(ciphertext)
          .await
          .map_err(|err| std::io::Error::new(std::io::ErrorKind::Other, err))?;
      }
      ThreadMessage::HasServerInput(ciphertext) => {
        let plaintext = session
          .decrypt(&ciphertext)
          .map_err(|err| std::io::Error::new(std::io::ErrorKind::Other, err))?;
        println!(
          "{}",
          String::from_utf8(plaintext)
            .map_err(|err| std::io::Error::new(std::io::ErrorKind::Other, err))?
        );
        cbox
          .session_save(&mut session)
          .map_err(|err| std::io::Error::new(std::io::ErrorKind::Other, err))?;
      }
      _ => {
        dbg!("Other message");
      }
    }
  }

  Ok(())
}
