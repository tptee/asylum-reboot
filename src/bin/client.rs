#![warn(clippy::all)]

use cryptobox::{store::file::FileStore, store::Store, CBox};
use proteus::keys::PreKeyId;

#[runtime::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync + 'static>> {
  let cbox = CBox::file_open("client_identity")
    .map_err(|err| std::io::Error::new(std::io::ErrorKind::NotFound, err))?;

  let pre_key_bundle = surf::get("http://0.0.0.0:9999/prekeys")
    .recv_bytes()
    .await?;

  let mut session = cbox.session_from_prekey("yeet".to_string(), &pre_key_bundle)?;

  let ciphertext = session.encrypt(b"hello")?;

  surf::post("http://0.0.0.0:9999/inbox")
    .body_bytes(ciphertext)
    .await?;

  Ok(())
}
