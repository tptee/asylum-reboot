#![warn(clippy::all)]
#![feature(custom_attribute)]

use cryptobox::{store::file::FileStore, store::Store, CBox};
use proteus::keys::PreKeyId;
use tide::{error::ResultExt, response, App, Context, EndpointResult};

async fn get_prekeys(cx: Context<CBox<FileStore>>) -> EndpointResult<Vec<u8>> {
  let pre_key_bundle = cx
    .state()
    .new_prekey(PreKeyId::new(0))
    .client_err()?
    .serialise()
    .client_err()?;

  Ok(pre_key_bundle)
}

async fn receive_message(mut cx: Context<CBox<FileStore>>) -> EndpointResult<()> {
  let ciphertext = cx.body_bytes().await.client_err()?;
  dbg!(String::from_utf8_lossy(&ciphertext));
  let (session, plaintext) = cx
    .state()
    .session_from_message("yeet".to_string(), &ciphertext)
    .client_err()?;
  dbg!(String::from_utf8(plaintext).client_err()?);
  Ok(())
}

fn main() -> Result<(), std::io::Error> {
  let cbox = CBox::file_open("test_identity")
    .map_err(|err| std::io::Error::new(std::io::ErrorKind::NotFound, err))?;
  let mut app = App::with_state(cbox);
  app.at("/prekeys").get(get_prekeys);
  app.at("/inbox").post(receive_message);
  Ok(app.run("0.0.0.0:9999")?)
}
